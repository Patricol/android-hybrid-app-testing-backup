# Android Hybrid App Testing

## Description
This project serves to deliver automated testing support for hybrid mobile application WebViews. It does this in an effort to overcome challenges in accurately extrapolating GUI widget coverage from the web components of hybrid Android applications through the implementation of various strategies for input generation and the extraction of WebView GUI components. In the future it aspires to elegantly deal with nuances related to the interplay between JavaScript web code and native Android Java code. 

Please see the [Wiki](https://gitlab.com/Patricol/android-hybrid-app-testing-backup/wikis/home) for more information!

## [Installation Instructions](https://gitlab.com/Patricol/android-hybrid-app-testing-backup/wikis/Installation)

## Usage
This section shows a basic usage scenario to help you get started fast. Please see the followng Wiki pages for more usage instructions and additional information.
- [GUI Overview and How-To](https://gitlab.com/Patricol/android-hybrid-app-testing-backup/wikis/GUI-Overview-and-How-To)
- [GUI Reference](https://gitlab.com/Patricol/android-hybrid-app-testing-backup/wikis/GUI-Reference)
- [API Overview and How-To](https://gitlab.com/Patricol/android-hybrid-app-testing-backup/wikis/API-Overview-and-How-To)

## Technologies Used
- Selendroid
- Java
- HTML5
- SQL
- Eclipse Java IDE
- Android Studio & SDK

## Credits
All of the work for this project was done by computer science students at the College of William and Mary.

Project Head: Professor Denys Poshyvanyk

Project Managers: Carlos Bernal-Cárdenas, Richard Bonett 

Developers: Adam Carpenter, Alexander MacDonald, Patrick Collins, Wentao Xu, Thomas Raddatz

## Changes

This is a copy of the [original repository](https://gitlab.com/WM-CSCI435-F17/Android-Hybrid-App-Testing); not a fork because (currently) forking does not include data like the wiki or issues.

As a result, the attribution for the issues is misleading; though all issues and edits do specify who actually created them; next to their timestamp.

Wiki pages that were copied from Javadoc have been removed; and links have been updated to point to this backup repository/wiki.

More compatibility information was added to the wiki.
